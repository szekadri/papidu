# Papidu

## About The Project

Papidu is a simple ios application created to help a local entrepreneur. Using the app, they can easily calculate the total price of the products purchased by the customer.

Functionalities:
* provides a user interface for adding products to the cart
* calculates the final amount
* calculates the amount of money returned
* stores the products sold in the database

### Built With

* XCode
* Swift
* Firebase
* CocoaPods
