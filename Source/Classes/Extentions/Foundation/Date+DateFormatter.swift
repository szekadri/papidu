//
//  String+DateFormatter.swift
//  Papidu
//
//  Created by Szekely Adrienn on 10.07.2021.
//

import Foundation

extension Date {
    func dateToId() -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = L10n.dateFormat
        return dateFormatter.string(from: Date.yesterday )
    }

    static var yesterday: Date { return Date().dayBefore }
    static var tomorrow:  Date { return Date().dayAfter }
    var dayBefore: Date {
        return Calendar.current.date(byAdding: .day, value: -1, to: noon)!
    }
    var dayAfter: Date {
        return Calendar.current.date(byAdding: .day, value: 1, to: noon)!
    }
    var noon: Date {
        return Calendar.current.date(bySettingHour: 12, minute: 0, second: 0, of: self)!
    }
}
