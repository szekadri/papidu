//
//  Layout.swift
//  Papidu
//
//  Created by Szekely Adrienn on 18.06.2021.
//

import struct CoreGraphics.CGFloat

public extension CGFloat {
    /// 2
    static let smallCornerRadius: CGFloat = 2
    /// 4
    static let cornerRadius: CGFloat = 4
    /// 6
    static let mediumCornerRadius: CGFloat = 6
    /// 8
    static let bigCornerRadius: CGFloat = 8

    /// 2
    static let smallPadding: CGFloat = 2
    /// 4
    static let halfPadding: CGFloat = 4
    /// 8
    static let padding: CGFloat = 8
    /// 16
    static let padding2x: CGFloat = padding * 2
    /// 24
    static let padding3x: CGFloat = padding * 3
    /// 32
    static let padding4x: CGFloat = padding * 4
    /// 48
    static let padding6x: CGFloat = padding * 6

    /// 0.45
    static let mediumOpacity: CGFloat = 0.45
    /// 1
    static let fullOpacity: CGFloat = 1.0


    /// 2
    static let spaceBetweenProducts: CGFloat = 2
    /// 120
    static let productImageSize: CGFloat = 120
    /// 60
    static let halfProductImageSize: CGFloat = 45
    /// 200
    static let productCellHeight: CGFloat = 245
    /// 24
    static let spaceBetweenPrices: CGFloat = 24
    /// 20
    static let baseCornerRadius: CGFloat = 20
    /// 200
    static let priceViewHeigth: CGFloat = 200
    /// 20
    static let buttonSize: CGFloat = 50
    /// 70
    static let cartCellHeigth: CGFloat = 60
}
