//
//  Fonts.swift
//  Papidu
//
//  Created by Szekely Adrienn on 24.06.2021.
//

import UIKit

// TODO: Use own font sizes and font family
enum FontSize: CGFloat {
    /// 12
    case extraSmall     = 12
    /// 14
    case mediumSmall    = 14
    /// 16
    case medium         = 16
    /// 18
    case medium2        = 18
    /// 24
    case large          = 24
    /// 32
    case large2         = 32
    /// 40
    case extraLarge     = 50
}

private struct FontFamily {

    enum WorkSans: String, FontConvertible {
        case bold             = "WorkSans-Bold"
        case medium           = "WorkSans-Medium"
        case regular          = "WorkSans-Regular"
    }
}

struct Font {
    static func bold(size: FontSize) -> UIFont {
        return FontFamily.WorkSans.bold.font(size: size)
    }

    static func medium(size: FontSize) -> UIFont {
        return FontFamily.WorkSans.medium.font(size: size)
    }

    static func regular(size: FontSize) -> UIFont {
        return FontFamily.WorkSans.regular.font(size: size)
    }
}

protocol FontConvertible {
    func font(size: FontSize) -> UIFont!
}

extension FontConvertible where Self: RawRepresentable, Self.RawValue == String {
    func font(size: FontSize) -> UIFont! {
        return UIFont(name: self.rawValue, size: size.rawValue)
    }
}
