//
//  GeneralConstants.swift
//  Papidu
//
//  Created by Szekely Adrienn on 18.06.2021.
//

import Foundation

enum AnimationDuration {
    static let quick: TimeInterval      = 0.15
    static let normal: TimeInterval     = 0.33
}

enum ReuseIdentifier {
    static let productCell = "cell.product"
    static let priceCell = "cell.price"
    static let cartCell = "cell.cart"
    static let simpleCell = "cell.simple"
}
