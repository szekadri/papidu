//
//  APIConstants.swift
//  Papidu
//
//  Created by Szekely Adrienn on 09.07.2021.
//

import Foundation

enum API {

    enum Path {
        static let products             = "/products"
        static let salesHistory         = "/salesHistory"
    }

    enum Param {
        static let name = "name"
        static let price = "price"
        static let quantity = "quantity"
        static let id = "id"
        static let image = "image"
        static let products = "products"
        static let product = "product"
        static let category = "category"
        static let productsReport = "productsReport"
        static let dateTime = "dateTime"
    }
}
