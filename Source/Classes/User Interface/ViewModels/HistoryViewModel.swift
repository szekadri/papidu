//
//  HistoryViewModel.swift
//  Papidu
//
//  Created by Szekely Adrienn on 18.07.2021.
//

import Foundation

protocol HistoryViewModel {
    var onErrorHandling : ((IncrementError) -> Void)? { get set }

    var cellModels: [SalesHistory] { get }

    func loadData(completion:@escaping (Bool) -> Void)

    func remove(_ index: Int, completion:@escaping () -> Void)
}

final class HistoryViewModelImpl: HistoryViewModel {

    var onErrorHandling : ((IncrementError) -> Void)?

    var cellModels: [SalesHistory] = []

    func loadData(completion:@escaping (Bool) -> Void) {
        FirestoreWorkRepository.getHistory(completion: { history in
            self.cellModels = history
            completion(true)
        },
        failure: { (error: IncrementError) in
            print("Fail")
            self.onErrorHandling?(error)
        })
    }

    func remove(_ index: Int, completion:@escaping () -> Void) {
        print(index)
        FirestoreWorkRepository.deleteHistory(history: cellModels[index],
            completion: {
                self.cellModels.remove(at: index)
                completion()
            },
            failure: { (error: IncrementError) in
                print("Fail")
                self.onErrorHandling?(error)
            })
    }
}
