//
//  PriceViewModel.swift
//  Papidu
//
//  Created by Szekely Adrienn on 22.06.2021.
//

import Foundation

protocol PriceViewModelDelegate: AnyObject {
    func reloadData()
}

protocol PriceViewModelFlowDelegate: AnyObject {
    func openCartView(products: [ProductReport])
    func closeCartView()
    func openReportView()
    func openHistoryView()
}

protocol PriceViewModel {
    var cellModels: [ProductReport] { get }

    func addToProduct(product: ProductReport)
}

final class PriceViewModelImpl: PriceViewModel {

    var cellModels: [ProductReport] = []

    var isEmpty: ((Bool) -> Void)?

    weak var priceDelegate: PriceViewModelDelegate?
    weak var flowDelegate: PriceViewModelFlowDelegate?

    init() {
        self.isEmpty?(true)
    }

    func addToProduct(product: ProductReport) {
        if let index = cellModels.firstIndex(of: product) {
            if product.quantity == 0 {
                cellModels.remove(at: index)
            } else {
                cellModels[index].quantity = product.quantity
            }
        } else {
            cellModels.append(product)
        }
        changeState()
    }

    func getTotal() -> Double {
        var price: Double = 0
        cellModels.forEach { cm in
            price += Double(cm.quantity) * cm.product.price
        }
        return price
    }

    func getTotal() -> String {
        var price: Double = 0
        cellModels.forEach { cm in
            price += Double(cm.quantity) * cm.product.price
        }
        return String(format: "%.2f RON", price)
    }

    func changeState() {
        if cellModels.isEmpty {
            self.isEmpty?(true)
        } else {
            self.isEmpty?(false)
        }
        priceDelegate?.reloadData()
    }

    private func emptyCellModels() {
        cellModels = []
        changeState()
    }


    func openCartView() {
        flowDelegate?.openCartView(products: cellModels)
    }

    func closeCartView() {
        flowDelegate?.closeCartView()
    }

    func saleFinished() {
        FirestoreWorkRepository.addToHistory(cartProducts: cellModels,
        failure: { (error: IncrementError) in
            print("Error while add To history! : \(error)")
        })
        flowDelegate?.closeCartView()
    }
}
