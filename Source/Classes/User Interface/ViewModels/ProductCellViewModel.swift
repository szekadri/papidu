//
//  ProductViewModel.swift
//  Papidu
//
//  Created by Szekely Adrienn on 18.06.2021.
//

import Foundation

protocol ProductCellViewModelFlowDelegate: AnyObject {
    func addToCart(product: ProductReport)
}

protocol ProductCellViewModel {
    var productReport: ProductReport {get}

    func addToCart(product: ProductReport)
}

final class ProductCellViewModelImpl: ProductCellViewModel {
    let productReport: ProductReport

    weak var flowDelegate: ProductCellViewModelFlowDelegate?

    init(product: ProductReport) {
        self.productReport = product
    }

    func addToCart(product: ProductReport) {
        flowDelegate?.addToCart(product: product)
    }
}
