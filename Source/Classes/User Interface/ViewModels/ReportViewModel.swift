//
//  ReportViewModel.swift
//  Papidu
//
//  Created by Szekely Adrienn on 13.07.2021.
//

import Foundation

protocol ReportViewModel {
    var onErrorHandling : ((IncrementError) -> Void)? { get set }

    var cellModels: [ProductReport] { get }

    func loadData(completion:@escaping (Bool) -> Void)
}

final class ReportViewModelImpl: ReportViewModel {

    var onErrorHandling : ((IncrementError) -> Void)?

    var cellModels: [ProductReport] = []

    func loadData(completion:@escaping (Bool) -> Void) {
        FirestoreWorkRepository.getProductReport(completion: { productReports in
            self.cellModels = productReports
            completion(true)
        },
        failure: { (error: IncrementError) in
            print("Fail")
            self.onErrorHandling?(error)
        })
    }

}
