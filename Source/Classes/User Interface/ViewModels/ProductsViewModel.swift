//
//  ProductsViewModel.swift
//  Papidu
//
//  Created by Szekely Adrienn on 18.06.2021.
//

import Foundation

protocol ProductsViewModelDelegate: AnyObject {
    func addToCart(product: ProductReport)
}

protocol ProductsViewModel {
    var onErrorHandling : ((IncrementError) -> Void)? { get set }

    var cellModels: [ProductCellViewModel] { get }

    func loadData(completion:@escaping (Bool) -> Void)

    func loadDataByCategory(category: String, completion:@escaping (Bool) -> Void)

    func addToCart(product: ProductReport)
}

final class ProductsViewModelImpl: ProductsViewModel {

    var onErrorHandling : ((IncrementError) -> Void)?

    var cellModels: [ProductCellViewModel] = []

    weak var delegate: ProductsViewModelDelegate?

    func loadData(completion:@escaping (Bool) -> Void) {
        FirestoreWorkRepository.getProducts(completion: { products in
            self.cellModels = products.compactMap{ product in
                ProductCellViewModelImpl(product: ProductReport(product: product, quantity: 0))
            }
            completion(true)
        },
        failure: { (error: IncrementError) in
            print("Fail")
            self.onErrorHandling?(error)
        })
    }

    func loadDataByCategory(category: String, completion:@escaping (Bool) -> Void) {
        FirestoreWorkRepository.getProducts(category: category ,completion: { products in
            self.cellModels = products.compactMap{ product in
                ProductCellViewModelImpl(product: ProductReport(product: product, quantity: 0))
            }
            completion(true)
        },
        failure: { (error: IncrementError) in
            print("Fail")
            self.onErrorHandling?(error)
        })
    }

    func addToCart(product: ProductReport) {
        delegate?.addToCart(product: product)
    }

}

