//
//  RootFlowController.swift
//  Papidu
//
//  Created by Szekely Adrienn on 18.06.2021.
//

import UIKit

class RootFlowController: FlowController {
    var presentationStyle: FlowPresentationStyle = .push

    // root flowcontroller will have always only one child flow
    var childFlows: [FlowController] = []

    var mainViewController: UIViewController? {
        return window?.rootViewController
    }

    // MARK: - Lifecycle

    init(window: UIWindow?) {
        self.window = window
    }

    func start() {
        // show landing screens or login screen or content screen, depening on autologin state
//        let flowController = MovieFlowController(from: self)
//        flowController.start()
        let orderFlowController = OrderFlowController(from: self)
        orderFlowController.start()
        let orderViewController = orderFlowController.navigationController
        setRoot(to: orderViewController)
    }

    func finish() {
        // root flowcontroller never finish
    }

    private weak var window: UIWindow?

    // MARK: - Set root

    private var root: UIViewController? {
        return window?.rootViewController
    }

    private func setRoot(to viewController: UIViewController?, animated: Bool = false) {
        guard let viewController = viewController else { return }

        guard root != viewController else { return }

        func changeRoot(to viewController: UIViewController) {
            window?.rootViewController = viewController
        }

        if animated {
            UIView.transition(with: window!, duration: AnimationDuration.normal, options: .transitionCrossDissolve, animations: {
                changeRoot(to: viewController)
            }, completion: nil)
        } else {
            changeRoot(to: viewController)
        }
    }

    // MARK: - Show flows

    private func show(flow: FlowController, animated: Bool = true) {
        flow.start()
    }
}
