//
//  ErrorView.swift
//  Papidu
//
//  Created by Szekely Adrienn on 10.07.2021.
//

import UIKit

class ErrorView: UIViewController {

    // MARK: - Private properties

    private var label: UILabel = {
        let label = UILabel()
        label.textColor = Asset.Colors.base.color
        label.font = Font.bold(size: .medium2)
        label.text = "Error"
        return label
    }()

    // MARK: - Init

    override func viewDidLoad() {
        super.viewDidLoad()

        initUI()
        initConstraints()
    }

    // MARK: - UI

    private func initUI() {
        view.backgroundColor = Asset.Colors.background.color

        view.addSubview(label)
    }

    private func initConstraints() {
        label.translatesAutoresizingMaskIntoConstraints = false

        let constraints: [NSLayoutConstraint] = [
            label.centerXAnchor.constraint(equalTo: view.centerXAnchor),
            label.centerYAnchor.constraint(equalTo: view.centerYAnchor, constant: -.padding6x)
        ]

        NSLayoutConstraint.activate(constraints)
    }
}
