//
//  FlowController.swift
//  Papidu
//
//  Created by Szekely Adrienn on 18.06.2021.
//

import UIKit

protocol FlowController: AnyObject {

    var childFlows: [FlowController] { get set }

    var mainViewController: UIViewController? { get }

    func start()
    func finish()
}

class BaseFlowController: FlowController, FlowControllerDelegate {

    weak var delegate: FlowControllerDelegate?

    var childFlows: [FlowController] = []

    private(set) var parentFlow: FlowController?

    var firstScreen: UIViewController {
        assertionFailure("Set rootViewController!")
        return UIViewController()
    }

    var mainViewController: UIViewController? {
        return firstScreen
    }

    // MARK: - Lifecycle

    required init(from parent: FlowController? = nil) {
        parentFlow = parent
        if let flow = parentFlow as? BaseFlowController {
            delegate = flow
        }
    }

    func start() {
        addToParent()
    }

    func finish() {
        delegate?.didFinish(on: self)
    }

    // MARK: - Helpers

    /**
     Add `self` to the parent flow.
     */
    private func addToParent() {
        parentFlow?.addChild(flow: self)
    }

    /**
     Removes `self` from the parent flow.
     */
    private func removeFromParent() {
        parentFlow?.removeChild(flow: self)
    }

    func didFinish(on flowController: FlowController) {
        removeFromParent()
    }
}

extension FlowController {

    /**
     Add `self` to the parent flow.
     */
    func addChild(flow: FlowController) {
        childFlows.append(flow)
    }

    /**
     Removes `self` from the parent flow.
     */
    func removeChild(flow: FlowController) {
        print(self, " - ", flow)
        if let idx = childFlows.firstIndex(where: { $0 === flow }) {
            childFlows.remove(at: idx)
        }
    }
}
