//
//  OrderFlowController.swift
//  Papidu
//
//  Created by Szekely Adrienn on 22.06.2021.
//

import UIKit
import FittedSheets

class OrderFlowController: NavigationFlowController {
    var orderViewController = OrderViewController()

    override var firstScreen: UIViewController {
        navigationController.isNavigationBarHidden = false
        navigationController.navigationBar.barTintColor = Asset.Colors.background.color
        navigationController.navigationBar.isTranslucent = false

        orderViewController.priceView = PriceTableView()
        let priceViewModel = PriceViewModelImpl()
        orderViewController.priceView.viewModel = priceViewModel
        priceViewModel.flowDelegate = self
        return orderViewController
    }
}

extension OrderFlowController: PriceViewModelFlowDelegate {
    func openReportView() {
        let reportViewController = ReportViewController()
        let reportViewModel = ReportViewModelImpl()
        reportViewController.viewModel = reportViewModel
        navigationController.pushViewController(reportViewController, animated: true)
    }

    func openCartView(products: [ProductReport]) {
        let cartViewController = CartViewController()
        let priceViewModel = PriceViewModelImpl()
        cartViewController.viewModel = priceViewModel
        cartViewController.viewModel.cellModels = products
        cartViewController.viewModel.flowDelegate = self
        navigationController.present(cartViewController, animated: true, completion: nil)
    }

    func closeCartView() {
        orderViewController.reloadData()
        navigationController.dismiss(animated: true, completion: {})
    }

    func openHistoryView() {
        let historyViewController = HistoryViewController()
        let historyViewModel = HistoryViewModelImpl()
        historyViewController.viewModel = historyViewModel
        navigationController.pushViewController(historyViewController, animated: true)
    }
}
