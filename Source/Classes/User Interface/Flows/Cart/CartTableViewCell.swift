//
//  CartTableViewCell.swift
//  Papidu
//
//  Created by Szekely Adrienn on 09.07.2021.
//

import UIKit

class CartTableViewCell: UITableViewCell {

    var viewModel: ProductReport? {
        didSet {
            updateUI()
        }
    }

    private var productImage: UIImageView = {
        let imageView = UIImageView()
        imageView.contentMode = .scaleAspectFill
        imageView.backgroundColor = .white
        imageView.layer.cornerRadius = .halfProductImageSize / 2
        imageView.clipsToBounds = true
        imageView.isUserInteractionEnabled = true
        return imageView
    }()

    private let nameLabel: UILabel = {
        let label = UILabel()
        label.textColor = Asset.Colors.base.color
        label.font = Font.regular(size: .medium)
        return label
    }()

    private let priceLabel: UILabel = {
        let label = UILabel()
        label.textColor = Asset.Colors.base.color
        label.font = Font.bold(size: .medium)
        return label
    }()

    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: .subtitle, reuseIdentifier: reuseIdentifier)
        initUI()
        initConstraints()
    }

    required init?(coder: NSCoder) {
        super.init(coder: coder)
        initUI()
        initConstraints()
    }

    private func initUI() {
        isUserInteractionEnabled = false

        addSubview(nameLabel)
        addSubview(priceLabel)
        addSubview(productImage)
    }

    private func initConstraints() {
        nameLabel.translatesAutoresizingMaskIntoConstraints = false
        priceLabel.translatesAutoresizingMaskIntoConstraints = false
        productImage.translatesAutoresizingMaskIntoConstraints = false

        let constraints = [
            productImage.centerYAnchor.constraint(equalTo: centerYAnchor),
            productImage.leadingAnchor.constraint(equalTo: leadingAnchor, constant: .padding3x),
            productImage.heightAnchor.constraint(equalToConstant: .halfProductImageSize),
            productImage.widthAnchor.constraint(equalToConstant: .halfProductImageSize),

            priceLabel.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -.padding3x),
            priceLabel.centerYAnchor.constraint(equalTo: centerYAnchor),

            nameLabel.centerYAnchor.constraint(equalTo: centerYAnchor),
            nameLabel.leadingAnchor.constraint(equalTo: productImage.trailingAnchor, constant: .padding3x),
        ]

        NSLayoutConstraint.activate(constraints)
    }

    private func updateUI() {
        if let vm = viewModel {
            nameLabel.text = vm.product.name
            priceLabel.text = "\(vm.quantity) x \(vm.product.price) RON"
            productImage.image = UIImage(named: vm.product.image)
        }
    }
}
