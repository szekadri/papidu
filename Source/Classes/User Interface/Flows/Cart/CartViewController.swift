//
//  CartViewController.swift
//  Papidu
//
//  Created by Szekely Adrienn on 22.06.2021.
//

import UIKit

class CartViewController: UIViewController {

    private var tableView = UITableView(frame: .zero)

    var viewModel: PriceViewModelImpl!

    private var finishButton: UIButton = {
        let button = UIButton()
        button.setTitle("   Finish   ", for: .normal)
        button.addTarget(self, action: #selector(finishButtonTapped), for: .touchUpInside)
        button.backgroundColor = Asset.Colors.priceView.color
        button.layer.cornerRadius = .bigCornerRadius
        return button
    }()

    private var closeButton: UIButton = {
        let button = UIButton()
        button.setImage(UIImage(systemName: "xmark"), for: .normal)
        button.addTarget(self, action: #selector(closeButtonTapped), for: .touchUpInside)
        return button
    }()

    private var textField: UITextField = {
        let textField = UITextField()
        textField.placeholder = "Tap here"
        textField.keyboardType = .numberPad
        textField.backgroundColor = .clear
        textField.borderStyle = .roundedRect
        return textField
    }()

    private var priceLabel: UILabel = {
        let label = UILabel()
        label.textColor = Asset.Colors.base.color
        label.font = Font.bold(size: .extraLarge)
        return label
    }()

    private var moneyBackLabel: UILabel = {
        let label = UILabel()
        label.textColor = Asset.Colors.base.color
        label.font = Font.bold(size: .extraLarge)
        label.text = ""
        return label
    }()

    private var simpleText = {(text: String) -> UILabel in
        let label = UILabel()
        label.textColor = Asset.Colors.base.color
        label.font = Font.regular(size: .mediumSmall)
        label.alpha = .mediumOpacity
        label.text = text
        return label
    }

    private var orderListLabel: UILabel = {
        let label = UILabel()
        label.textColor = Asset.Colors.base.color
        label.font = Font.bold(size: .medium2)
        label.text = "Order List"
        return label
    }()

    private var totalText: UILabel!
    private var changeText: UILabel!


    override func viewDidLoad() {
        super.viewDidLoad()
        isModalInPresentation = true

        guard viewModel != nil else {
            assertionFailure("View model is not set for \(self)")
            return
        }

        initUI()
        initConstraints()
    }

    private func initUI() {

        view.backgroundColor = Asset.Colors.background.color
        tableView.backgroundColor = Asset.Colors.background.color
        tableView.separatorStyle = .none
        tableView.dataSource = self
        tableView.delegate = self
        tableView.register(CartTableViewCell.self, forCellReuseIdentifier: ReuseIdentifier.cartCell)
        hideKeyboardWhenTappedAround()
        priceLabel.text = viewModel.getTotal()

        totalText = simpleText("Total")
        changeText = simpleText("")

        view.addSubview(tableView)
        view.addSubview(finishButton)
        view.addSubview(textField)
        view.addSubview(moneyBackLabel)
        view.addSubview(closeButton)
        view.addSubview(priceLabel)
        view.addSubview(totalText)
        view.addSubview(changeText)
        view.addSubview(orderListLabel)
    }

    private func initConstraints() {
        tableView.translatesAutoresizingMaskIntoConstraints = false
        finishButton.translatesAutoresizingMaskIntoConstraints = false
        textField.translatesAutoresizingMaskIntoConstraints = false
        moneyBackLabel.translatesAutoresizingMaskIntoConstraints = false
        closeButton.translatesAutoresizingMaskIntoConstraints = false
        priceLabel.translatesAutoresizingMaskIntoConstraints = false
        totalText.translatesAutoresizingMaskIntoConstraints = false
        changeText.translatesAutoresizingMaskIntoConstraints = false
        orderListLabel.translatesAutoresizingMaskIntoConstraints = false

        let constraints = [
            closeButton.topAnchor.constraint(equalTo: view.topAnchor, constant: .padding),
            closeButton.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: -.padding),
            closeButton.heightAnchor.constraint(equalToConstant: .buttonSize),
            closeButton.widthAnchor.constraint(equalToConstant: .buttonSize),

            totalText.topAnchor.constraint(equalTo: closeButton.bottomAnchor, constant: .padding2x),
            totalText.centerXAnchor.constraint(equalTo: view.centerXAnchor),

            priceLabel.topAnchor.constraint(equalTo: totalText.bottomAnchor),
            priceLabel.centerXAnchor.constraint(equalTo: view.centerXAnchor),

            changeText.topAnchor.constraint(equalTo: priceLabel.bottomAnchor, constant: .padding3x),
            changeText.centerXAnchor.constraint(equalTo: view.centerXAnchor),

            moneyBackLabel.topAnchor.constraint(equalTo: changeText.bottomAnchor),
            moneyBackLabel.centerXAnchor.constraint(equalTo: view.centerXAnchor),

            textField.bottomAnchor.constraint(equalTo: view.centerYAnchor, constant: -2 * .padding4x),
            textField.centerXAnchor.constraint(equalTo: view.centerXAnchor),
            textField.heightAnchor.constraint(equalToConstant: .padding6x),

            orderListLabel.topAnchor.constraint(equalTo: textField.bottomAnchor, constant: .padding4x),
            orderListLabel.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: .padding3x),

            tableView.topAnchor.constraint(equalTo: orderListLabel.bottomAnchor, constant: .padding3x),
            tableView.bottomAnchor.constraint(equalTo: view.bottomAnchor, constant: -2 * .padding6x),
            tableView.leadingAnchor.constraint(equalTo: view.leadingAnchor),
            tableView.trailingAnchor.constraint(equalTo: view.trailingAnchor),

            finishButton.topAnchor.constraint(equalTo: tableView.bottomAnchor),
            finishButton.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: -.padding4x),
            finishButton.heightAnchor.constraint(equalToConstant: .padding6x),
            finishButton.widthAnchor.constraint(equalToConstant: 3 * .padding6x)
        ]

        NSLayoutConstraint.activate(constraints)
    }

    @objc private func finishButtonTapped(){
        viewModel.saleFinished()
    }

    @objc private func closeButtonTapped(){
        viewModel.closeCartView()
    }

    func hideKeyboardWhenTappedAround() {
        let tap = UITapGestureRecognizer(target: self, action: #selector(dismissKeyboard))
        tap.cancelsTouchesInView = false
        view.addGestureRecognizer(tap)
    }

    @objc func dismissKeyboard() {
        view.endEditing(true)
        if let text = textField.text {
            if let money = Double(text) {
                let moneyBack = money - viewModel.getTotal()
                moneyBackLabel.text = String(format: "%.2f", moneyBack)
                changeText.text = "Rest"
            }
        }
    }
}

extension CartViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: ReuseIdentifier.cartCell, for: indexPath) as? CartTableViewCell else {
            return UITableViewCell()
        }
        cell.backgroundColor = Asset.Colors.background.color

        cell.viewModel = viewModel.cellModels[indexPath.row]

        return cell
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return viewModel.cellModels.count
    }

    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return .cartCellHeigth
    }
}

extension CartViewController: UITableViewDelegate {

}
