//
//  PriceViewController.swift
//  Papidu
//
//  Created by Szekely Adrienn on 21.06.2021.
//

import UIKit

protocol PriceTableViewDelegate: AnyObject {
    func reloadOrderViewController()
}

class PriceTableView: UIViewController {

    private var tableView = UITableView(frame: .zero)

    var viewModel: PriceViewModelImpl!
    weak var delegate: PriceTableViewDelegate?


    var contentView: UIView = {
        let view = UIView()
        view.layer.cornerRadius = .baseCornerRadius
        view.layer.maskedCorners = [.layerMaxXMinYCorner, .layerMinXMinYCorner]
        view.backgroundColor = Asset.Colors.priceView.color
        return view
    }()

    var priceLabel: UILabel = {
        let label = UILabel()
        label.textColor = Asset.Colors.background.color
        label.font = Font.bold(size: .extraLarge)
        return label
    }()

    var nextButton: UIButton = {
        let button = UIButton()
        button.backgroundColor = Asset.Colors.iconColor.color
        button.setImage(UIImage(systemName: "chevron.right"), for: .normal)
        button.layer.shadowColor = UIColor.black.cgColor
        button.layer.shadowOffset = .zero
        button.layer.shadowOpacity = 0.1
        button.layer.cornerRadius = .buttonSize / 2
        button.addTarget(self, action: #selector(nextButtonTapped), for: .touchUpInside)
        return button
    }()

    var closeButton: UIButton = {
        let button = UIButton()
        button.setImage(UIImage(systemName: "xmark"), for: .normal)
        button.addTarget(self, action: #selector(closeButtonTapped), for: .touchUpInside)
        return button
    }()

    override func viewDidLoad() {
        super.viewDidLoad()

        guard viewModel != nil else {
            assertionFailure("View model is not set for \(self)")
            return
        }
        
        initUI()
        initConstraints()
    }

    private func initUI() {
        view.isOpaque = false
        view.layer.cornerRadius = .baseCornerRadius
        view.layer.maskedCorners = [.layerMaxXMinYCorner, .layerMinXMinYCorner]
        tableView.backgroundColor = Asset.Colors.priceView.color
        tableView.separatorStyle = .none
        tableView.dataSource = self
        tableView.delegate = self
        tableView.register(PriceTableViewCell.self, forCellReuseIdentifier: ReuseIdentifier.priceCell)
        tableView.showsVerticalScrollIndicator = false
        viewModel.priceDelegate = self

        view.addSubview(contentView)
        contentView.addSubview(tableView)
        contentView.addSubview(priceLabel)
        contentView.addSubview(nextButton)
        contentView.addSubview(closeButton)
    }

    private func initConstraints() {
        tableView.translatesAutoresizingMaskIntoConstraints = false
        priceLabel.translatesAutoresizingMaskIntoConstraints = false
        nextButton.translatesAutoresizingMaskIntoConstraints = false
        contentView.translatesAutoresizingMaskIntoConstraints = false
        closeButton.translatesAutoresizingMaskIntoConstraints = false

        let constraints = [
            contentView.topAnchor.constraint(equalTo: view.topAnchor),
            contentView.bottomAnchor.constraint(equalTo: view.bottomAnchor),
            contentView.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: .padding),
            contentView.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: -.padding),

            priceLabel.topAnchor.constraint(equalTo: contentView.topAnchor),
            priceLabel.leadingAnchor.constraint(equalTo: contentView.leadingAnchor, constant: .padding4x),

            closeButton.topAnchor.constraint(equalTo: contentView.topAnchor),
            closeButton.trailingAnchor.constraint(equalTo: contentView.trailingAnchor, constant: -.padding2x),

            tableView.topAnchor.constraint(equalTo: priceLabel.bottomAnchor),
            tableView.bottomAnchor.constraint(equalTo: contentView.bottomAnchor, constant: -.padding6x),
            tableView.leadingAnchor.constraint(equalTo: contentView.leadingAnchor, constant: .padding4x),
            tableView.trailingAnchor.constraint(equalTo: contentView.trailingAnchor, constant: -.padding6x - .padding4x),

            nextButton.topAnchor.constraint(equalTo: tableView.bottomAnchor, constant: -.padding3x),
            nextButton.trailingAnchor.constraint(equalTo: contentView.trailingAnchor, constant: -.padding3x),
            nextButton.heightAnchor.constraint(equalToConstant: .buttonSize),
            nextButton.widthAnchor.constraint(equalToConstant: .buttonSize)
        ]

        NSLayoutConstraint.activate(constraints)
    }

    private func reloadPrice() {
        priceLabel.text = viewModel.getTotal()
    }

    @objc private func nextButtonTapped(){
        viewModel.openCartView()
    }

    @objc private func closeButtonTapped(){
        delegate?.reloadOrderViewController()
    }
}

extension PriceTableView: UITableViewDataSource {
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: ReuseIdentifier.priceCell, for: indexPath) as? PriceTableViewCell else {
            return UITableViewCell()
        }

        cell.viewModel = viewModel.cellModels[indexPath.row]
        cell.backgroundColor = Asset.Colors.priceView.color
        
        return cell
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return viewModel.cellModels.count
    }

    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return .padding2x
    }
}

extension PriceTableView: UITableViewDelegate {
    
}

extension PriceTableView: PriceViewModelDelegate {
    func reloadData() {
        tableView.reloadData()
        reloadPrice()
    }
}
