//
//  PriceTableViewCell.swift
//  Papidu
//
//  Created by Szekely Adrienn on 22.06.2021.
//

import UIKit

class PriceTableViewCell: UITableViewCell {

    var viewModel: ProductReport? {
        didSet {
            updateUI()
        }
    }

    private let nameLabel: UILabel = {
        let label = UILabel()
        label.textColor = Asset.Colors.background.color
        label.font = Font.regular(size: .mediumSmall)
        return label
    }()

    private let priceLabel: UILabel = {
        let label = UILabel()
        label.textColor = Asset.Colors.background.color
        label.font = Font.regular(size: .mediumSmall)
        return label
    }()

    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: .subtitle, reuseIdentifier: reuseIdentifier)
        initUI()
        initConstraints()
    }

    required init?(coder: NSCoder) {
        super.init(coder: coder)
        initUI()
        initConstraints()
    }

    private func initUI() {
        isUserInteractionEnabled = false
        
        addSubview(nameLabel)
        addSubview(priceLabel)
    }

    private func initConstraints() {
        nameLabel.translatesAutoresizingMaskIntoConstraints = false
        priceLabel.translatesAutoresizingMaskIntoConstraints = false

        let constraints = [
            priceLabel.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -.padding2x),
            priceLabel.centerYAnchor.constraint(equalTo: centerYAnchor),

            nameLabel.centerYAnchor.constraint(equalTo: centerYAnchor),
            nameLabel.leadingAnchor.constraint(equalTo: leadingAnchor, constant: .padding2x),
        ]

        NSLayoutConstraint.activate(constraints)
    }

    private func updateUI() {
        if let vm = viewModel {
            nameLabel.text = vm.product.name
            priceLabel.text = "\(vm.quantity) x \(vm.product.price) RON"
        }
    }
}
