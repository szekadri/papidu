//
//  ProductTableViewCell.swift
//  Papidu
//
//  Created by Szekely Adrienn on 18.06.2021.
//

import UIKit
import ValueStepper
import Kingfisher

class ProductCollectionViewCell: UICollectionViewCell {

    var viewModel: ProductCellViewModelImpl? {
        didSet {
            updateUI()
        }
    }

    private let cellContentView: UIView = {
        let view = UIView()
        view.backgroundColor = Asset.Colors.iconColor.color
        view.layer.cornerRadius = .baseCornerRadius
        view.clipsToBounds = true
        view.layer.masksToBounds = false
        view.layer.shadowColor = UIColor.black.cgColor
        view.layer.shadowOpacity = 0.1
        view.layer.shadowOffset = .zero
        return view
    }()

    private let nameLabel: UILabel = {
        let label = UILabel()
        label.textColor = Asset.Colors.base.color
        return label
    }()

    private let priceLabel: UILabel = {
        let label = UILabel()
        label.textColor = Asset.Colors.base.color
        return label
    }()

    private var productImage: UIImageView = {
        let imageView = UIImageView()
        imageView.contentMode = .scaleAspectFill
        imageView.backgroundColor = .white
        imageView.layer.cornerRadius = .productImageSize / 2
        imageView.clipsToBounds = true
        imageView.isUserInteractionEnabled = true
        return imageView
    }()

    private let stepper: ValueStepper = {
        let numberFormatter = NumberFormatter()
        numberFormatter.numberStyle = .decimal
        numberFormatter.maximumFractionDigits = 0
        let stepper = ValueStepper()
        stepper.tintColor = Asset.Colors.base.color
        stepper.backgroundColor = .clear
        stepper.labelTextColor = Asset.Colors.base.color
        stepper.autorepeat = true
        stepper.maximumValue = 20
        stepper.minimumValue = 0
        stepper.stepValue = 1
        stepper.numberFormatter = numberFormatter
        return stepper
    }()

    override init(frame: CGRect) {
        super.init(frame: frame)
        initUI()
        initConstraints()
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        initUI()
        initConstraints()
    }

    private func initUI() {
        backgroundColor = Asset.Colors.background.color

        productImage.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(tappedOnImage)))
        stepper.addTarget(self, action: #selector(valueChanged), for: .valueChanged)

        addSubview(cellContentView)
        cellContentView.addSubview(nameLabel)
        cellContentView.addSubview(stepper)
        cellContentView.addSubview(priceLabel)
        cellContentView.addSubview(productImage)
    }

    private func initConstraints() {
        cellContentView.translatesAutoresizingMaskIntoConstraints = false
        nameLabel.translatesAutoresizingMaskIntoConstraints = false
        stepper.translatesAutoresizingMaskIntoConstraints = false
        productImage.translatesAutoresizingMaskIntoConstraints = false
        priceLabel.translatesAutoresizingMaskIntoConstraints = false

        let constraints = [
            cellContentView.topAnchor.constraint(equalTo: topAnchor, constant: .padding),
            cellContentView.bottomAnchor.constraint(equalTo: bottomAnchor, constant: -.padding),
            cellContentView.leadingAnchor.constraint(equalTo: leadingAnchor, constant: .padding),
            cellContentView.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -.padding),

            stepper.centerXAnchor.constraint(equalTo: cellContentView.centerXAnchor),
            stepper.bottomAnchor.constraint(equalTo:cellContentView.bottomAnchor, constant: -.padding),

            priceLabel.centerXAnchor.constraint(equalTo: cellContentView.centerXAnchor),
            priceLabel.bottomAnchor.constraint(equalTo: stepper.topAnchor, constant: -.padding),

            nameLabel.centerXAnchor.constraint(equalTo: cellContentView.centerXAnchor),
            nameLabel.bottomAnchor.constraint(equalTo: priceLabel.topAnchor, constant: -.padding),

            productImage.centerXAnchor.constraint(equalTo: cellContentView.centerXAnchor),
            productImage.bottomAnchor.constraint(equalTo: nameLabel.topAnchor, constant: -.padding),
            productImage.heightAnchor.constraint(equalToConstant: .productImageSize),
            productImage.widthAnchor.constraint(equalToConstant: .productImageSize)
        ]

        NSLayoutConstraint.activate(constraints)
    }

    private func updateUI() {
        if let model = viewModel {
            productImage.kf.setImage(with: URL(string: model.productReport.product.image))
            priceLabel.text = String(describing: model.productReport.product.price) + " RON"
            nameLabel.text = model.productReport.product.name
            stepper.value = Double(model.productReport.quantity)
        }
    }

    @objc private func tappedOnImage() {
        stepper.value += 1
    }

    @objc private func valueChanged() {
        if let model = viewModel {
            model.productReport.quantity = Int(stepper.value)
            model.addToCart(product: model.productReport)
        }
    }
}
