//
//  CategoryButton.swift
//  Papidu
//
//  Created by Szekely Adrienn on 18.07.2021.
//

import UIKit

class CategoryButton: UIButton {

    private let spaceVertical: CGFloat = 2
    private let spaceHorizontal: CGFloat = 6
    private var label: UILabel = {
        let label = UILabel()
        label.font = Font.medium(size: .extraSmall)
        label.textColor = Asset.Colors.base.color
        return label
    }()

    required init(text: String) {
        super.init(frame: CGRect.zero)
        label.text = text
        initUI()
        initConstraints()
    }
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    // MARK: - UI

    private func initUI() {
        backgroundColor = Asset.Colors.iconColor.color
        layer.cornerRadius = 4.0
        addSubview(label)
    }

    private func initConstraints() {
        label.translatesAutoresizingMaskIntoConstraints = false
        let constraints = [
            label.topAnchor.constraint(equalTo: topAnchor, constant: spaceVertical),
            label.bottomAnchor.constraint(equalTo: bottomAnchor, constant: -spaceVertical),
            label.leadingAnchor.constraint(equalTo: leadingAnchor, constant: spaceHorizontal),
            label.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -spaceHorizontal)
        ]
        NSLayoutConstraint.activate(constraints)
    }
}
