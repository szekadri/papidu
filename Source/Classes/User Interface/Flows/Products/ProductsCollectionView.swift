//
//  ProductsViewController.swift
//  Papidu
//
//  Created by Szekely Adrienn on 18.06.2021.
//

import UIKit

class ProductsCollectionView: UIViewController {

    private var collectionView = UICollectionView(frame: .zero, collectionViewLayout: UICollectionViewFlowLayout())

    var viewModel: ProductsViewModel!

    override func viewDidLoad() {
        super.viewDidLoad()

        guard viewModel != nil else {
            assertionFailure("View model is not set for \(self)")
            return
        }

        initUI()
        initConstraints()
        loadData()
    }

    func loadData() {
        viewModel.loadData { [weak self] _ in
            self?.collectionView.reloadData()
        }

        viewModel.onErrorHandling = { error in
            print("Errorr!!!!")
            let errorView = ErrorView()
            self.add(errorView)
        }
    }

    func loadDataByCategory(category: String) {
        viewModel.loadDataByCategory(category: category) { [weak self] _ in
            self?.collectionView.reloadData()
        }
    }

    private func initUI() {
        view.backgroundColor = Asset.Colors.background.color
        collectionView.backgroundColor = Asset.Colors.background.color
        collectionView.dataSource = self
        collectionView.delegate = self
        collectionView.register(ProductCollectionViewCell.self, forCellWithReuseIdentifier: ReuseIdentifier.productCell)
        collectionView.showsVerticalScrollIndicator = false

        view.addSubview(collectionView)
    }

    private func initConstraints() {
        collectionView.translatesAutoresizingMaskIntoConstraints = false

        let constraints = [
            collectionView.topAnchor.constraint(equalTo: view.topAnchor),
            collectionView.bottomAnchor.constraint(equalTo: view.bottomAnchor),
            collectionView.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: .padding),
            collectionView.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: -.padding)
        ]

        NSLayoutConstraint.activate(constraints)
    }
}

extension ProductsCollectionView: UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return viewModel.cellModels.count
    }

    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {

        guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: ReuseIdentifier.productCell, for: indexPath) as? ProductCollectionViewCell else {
            return UICollectionViewCell()
        }

        let viewModel = ProductCellViewModelImpl(product: viewModel.cellModels[indexPath.row].productReport)

        cell.viewModel = viewModel
        cell.viewModel?.flowDelegate = self

        return cell
    }

}

extension ProductsCollectionView: UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets(top: 0, left: .spaceBetweenProducts, bottom: .padding2x, right: .spaceBetweenProducts)
    }

    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return .spaceBetweenProducts
    }

    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return .spaceBetweenProducts
    }

    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: view.frame.size.width/2 - 2 * .spaceBetweenProducts - .padding, height: .productCellHeight)
    }
}

extension ProductsCollectionView: UICollectionViewDelegate {

}

extension ProductsCollectionView: ProductCellViewModelFlowDelegate {
    func addToCart(product: ProductReport) {
        viewModel.addToCart(product: product)
    }
}
