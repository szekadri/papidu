//
//  ReportViewController.swift
//  Papidu
//
//  Created by Szekely Adrienn on 13.07.2021.
//

import UIKit

class ReportViewController: UIViewController {

    private var tableView = UITableView(frame: .zero)

    var viewModel: ReportViewModel!

    override func viewDidLoad() {
        super.viewDidLoad()
        isModalInPresentation = true

        guard viewModel != nil else {
            assertionFailure("View model is not set for \(self)")
            return
        }

        initUI()
        initConstraints()
        loadData()
    }

    private func loadData() {
        viewModel.loadData { [weak self] _ in
            self?.tableView.reloadData()
        }
    }

    private func initUI() {
        view.backgroundColor = Asset.Colors.background.color
        tableView.backgroundColor = Asset.Colors.background.color
        tableView.separatorStyle = .none
        tableView.dataSource = self
        tableView.delegate = self
        tableView.register(UITableViewCell.self, forCellReuseIdentifier: ReuseIdentifier.simpleCell)
        view.addSubview(tableView)
    }

    private func initConstraints() {
        tableView.translatesAutoresizingMaskIntoConstraints = false

        let constraints = [

            tableView.topAnchor.constraint(equalTo: view.topAnchor),
            tableView.bottomAnchor.constraint(equalTo: view.bottomAnchor),
            tableView.leadingAnchor.constraint(equalTo: view.leadingAnchor),
            tableView.trailingAnchor.constraint(equalTo: view.trailingAnchor)
        ]

        NSLayoutConstraint.activate(constraints)
    }
}

extension ReportViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let cell = tableView.dequeueReusableCell(withIdentifier: ReuseIdentifier.simpleCell, for: indexPath)

        cell.backgroundColor = Asset.Colors.background.color

        cell.textLabel?.text = "\(viewModel.cellModels[indexPath.row].product.name)   \(viewModel.cellModels[indexPath.row].quantity)"

        return cell
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return viewModel.cellModels.count
    }
}

extension ReportViewController: UITableViewDelegate {

}
