//
//  Products1VewController.swift
//  Papidu
//
//  Created by Szekely Adrienn on 21.06.2021.
//

import UIKit

class OrderViewController: UIViewController {

    var contentView: UIView = {
        let view = UIView()
        view.backgroundColor = .clear
        return view
    }()

    var priceView: PriceTableView!

    var productsCollectionView: ProductsCollectionView = {
        let view = ProductsCollectionView()
        return view
    }()


    var papiduLabel: UILabel = {
        let label = UILabel()
        label.font = Font.bold(size: .large2)
        label.text = "Papidu"
        label.textColor = Asset.Colors.base.color
        return label
    }()

    var reportButton: UIButton = {
        let button = UIButton()
        button.setImage(UIImage(systemName: "info.circle.fill"), for: .normal)
        return button
    }()

    var historyButton: UIButton = {
        let button = UIButton()
        button.setImage(UIImage(systemName: "list.bullet"), for: .normal)
        return button
    }()

    private var foodCategory: CategoryButton {
        let button = CategoryButton(text: L10n.food)
        button.addTarget(self, action: #selector(foodCategoryButtonTapped), for: .touchUpInside)
        return button
    }

    private var drinkCategory: CategoryButton {
        let button = CategoryButton(text: L10n.drink)
        button.addTarget(self, action: #selector(drinkCategoryButtonTapped), for: .touchUpInside)
        return button
    }

    private var cocktailCategory: CategoryButton {
        let button = CategoryButton(text: L10n.cocktail)
        button.addTarget(self, action: #selector(cocktailCategoryButtonTapped), for: .touchUpInside)
        return button
    }

    private var shotCategory: CategoryButton {
        let button = CategoryButton(text: L10n.shot)
        button.addTarget(self, action: #selector(shotCategoryButtonTapped), for: .touchUpInside)
        return button
    }

    private var menuCategory: CategoryButton {
        let button = CategoryButton(text: L10n.menu)
        button.addTarget(self, action: #selector(menuCategoryButtonTapped), for: .touchUpInside)
        return button
    }

    private var categoryStackView: UIStackView = {
        let stackView = UIStackView()
        stackView.axis = .horizontal
        stackView.alignment = .center
        stackView.distribution = .equalSpacing
        stackView.spacing = .padding2x
        return stackView
    }()

    var priceViewConstraint: NSLayoutConstraint!
    var productsViewConstraint: NSLayoutConstraint!

    override func viewDidLoad() {
        super.viewDidLoad()

        guard priceView != nil else {
            assertionFailure("View model is not set for \(self)")
            return
        }

        priceView.viewModel.isEmpty = { isEmpty in
            if isEmpty {
                self.hidePriceView()
            } else {
                self.showPriceView()
            }
        }
        priceView.delegate = self

        initUI()
        initConstraints()
    }

    func reloadData() {
        hidePriceView()
        priceView.viewModel.cellModels = []
        productsCollectionView.loadData()
    }

    private func initUI() {
        navigationItem.leftBarButtonItem = UIBarButtonItem.init(customView: papiduLabel)
        navigationItem.rightBarButtonItem = UIBarButtonItem.init(customView: reportButton)
        navigationItem.rightBarButtonItems = [
            UIBarButtonItem.init(customView: reportButton),
            UIBarButtonItem.init(customView: historyButton)
        ]
        

        let productsViewModel = ProductsViewModelImpl()
        productsCollectionView.viewModel = productsViewModel
        productsViewModel.delegate = self

        view.backgroundColor = Asset.Colors.background.color
        priceView.view.isHidden = true
        reportButton.addTarget(self, action: #selector(reportButtonTapped), for: .touchUpInside)
        historyButton.addTarget(self, action: #selector(historyButtonTapped), for: .touchUpInside)

        view.addSubview(contentView)
        contentView.addSubview(categoryStackView)
        contentView.addSubview(productsCollectionView.view)
        contentView.addSubview(priceView.view)

        categoryStackView.addArrangedSubview(foodCategory)
        categoryStackView.addArrangedSubview(shotCategory)
        categoryStackView.addArrangedSubview(menuCategory)
        categoryStackView.addArrangedSubview(cocktailCategory)
        categoryStackView.addArrangedSubview(drinkCategory)
    }

    private func initConstraints() {
        contentView.translatesAutoresizingMaskIntoConstraints = false
        productsCollectionView.view.translatesAutoresizingMaskIntoConstraints = false
        priceView.view.translatesAutoresizingMaskIntoConstraints = false
        categoryStackView.translatesAutoresizingMaskIntoConstraints = false

        productsViewConstraint = productsCollectionView.view.bottomAnchor.constraint(equalTo: contentView.bottomAnchor)
        priceViewConstraint = productsCollectionView.view.bottomAnchor.constraint(equalTo: contentView.bottomAnchor, constant: -.priceViewHeigth + .padding2x)

        let constraints = [
            contentView.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor),
            contentView.leadingAnchor.constraint(equalTo: view.leadingAnchor),
            contentView.trailingAnchor.constraint(equalTo: view.trailingAnchor),
            contentView.bottomAnchor.constraint(equalTo: view.bottomAnchor),

            categoryStackView.topAnchor.constraint(equalTo: contentView.topAnchor, constant: .padding2x),
            categoryStackView.leadingAnchor.constraint(equalTo: contentView.leadingAnchor, constant: .padding2x),

            productsCollectionView.view.topAnchor.constraint(equalTo: categoryStackView.bottomAnchor, constant: .padding),
            productsCollectionView.view.leadingAnchor.constraint(equalTo: contentView.leadingAnchor),
            productsCollectionView.view.trailingAnchor.constraint(equalTo: contentView.trailingAnchor),
            productsViewConstraint!,

            priceView.view.topAnchor.constraint(equalTo: contentView.bottomAnchor, constant: -.priceViewHeigth),
            priceView.view.leadingAnchor.constraint(equalTo: contentView.leadingAnchor),
            priceView.view.trailingAnchor.constraint(equalTo: contentView.trailingAnchor),
            priceView.view.bottomAnchor.constraint(equalTo: contentView.bottomAnchor)
        ]

        NSLayoutConstraint.activate(constraints)
    }

    @objc func reportButtonTapped(){
        priceView.viewModel.flowDelegate?.openReportView()
    }

    @objc func historyButtonTapped(){
        priceView.viewModel.flowDelegate?.openHistoryView()
    }

    @objc func foodCategoryButtonTapped(){
        print("foodCategoryButtonTapped")
        productsCollectionView.loadDataByCategory(category: L10n.food)
    }

    @objc func shotCategoryButtonTapped(){
        print("foodCategoryButtonTapped")
        productsCollectionView.loadDataByCategory(category: L10n.shot)
    }

    @objc func drinkCategoryButtonTapped(){
        print("foodCategoryButtonTapped")
        productsCollectionView.loadDataByCategory(category: L10n.drink)
    }

    @objc func menuCategoryButtonTapped(){
        print("foodCategoryButtonTapped")
        productsCollectionView.loadDataByCategory(category: L10n.menu)
    }

    @objc func cocktailCategoryButtonTapped(){
        print("foodCategoryButtonTapped")
        productsCollectionView.loadDataByCategory(category: L10n.cocktail)
    }

    func hidePriceView() {
        priceView.view.isHidden = true

        priceViewConstraint.isActive = false
        productsViewConstraint.isActive = true
    }

    func showPriceView() {
        priceView.view.isHidden = false

        productsViewConstraint.isActive = false
        priceViewConstraint.isActive = true
    }
}

extension OrderViewController: ProductsViewModelDelegate {
    func addToCart(product: ProductReport) {
        priceView.viewModel?.addToProduct(product: product)
    }
}

extension OrderViewController: PriceTableViewDelegate {
    func reloadOrderViewController() {
        reloadData()
    }
}
