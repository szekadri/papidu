//
//  AppEngine.swift
//  Papidu
//
//  Created by Szekely Adrienn on 09.07.2021.
//

import Foundation
import Firebase

class AppEngine {

    // MARK: - Lifecycle

    init() {
        FirebaseApp.configure()
        // TODO: Setup logging and Fabric
//        Logger.setupLogging()
//        Fabric.with([Crashlytics.self])
    }

    // MARK: - Application events

    func prepareAppStart() {
        startDebugToolsIfNeeded()
    }
}

extension AppEngine {

    fileprivate func startDebugToolsIfNeeded() {
        // TODO: Start debugging tools (netfox for example)
//        if AppLaunchConfig.useNetfox {
//            NFX.sharedInstance().start()
//        }
    }
}
