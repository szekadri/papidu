//
//  FirestoreWorkRepository.swift
//  Papidu
//
//  Created by Szekely Adrienn on 09.07.2021.
//

import Foundation
import FirebaseFirestoreSwift
import FirebaseFirestore

class FirestoreWorkRepository: WorkRepositoryProtocol {

    private static let firestore = Firestore.firestore()
    private static let productsCollectionRefference = firestore.collection(API.Path.products)
    private static let salesHistory = firestore.collection(API.Path.salesHistory)

    static func getProducts(completion: @escaping ([Product]) -> Void, failure: @escaping (IncrementError) -> Void) {
        print("In getProducts")

        productsCollectionRefference.getDocuments { querySnapshot, error in
            if let error = error {
                print("Error getting documents: \(error)")
                failure(.databaseError(error: L10n.errorGettingDocuments))
                return
            }
            guard let documents = querySnapshot?.documents else {
                print("No projects found")
                completion([])
                return
            }
            let result: [Product] = documents.compactMap { (queryDocumentSnapshot: QueryDocumentSnapshot) -> Product? in
                return try? queryDocumentSnapshot.data(as: Product.self)
            }.sorted(by: { $0.category < $1.category })
            completion(result)
        }
    }

    static func getProducts(category: String, completion: @escaping ([Product]) -> Void, failure: @escaping (IncrementError) -> Void) {
        print("In getProducts")

        productsCollectionRefference.whereField(API.Param.category, isEqualTo:  category).getDocuments { querySnapshot, error in
            if let error = error {
                print("Error getting documents: \(error)")
                failure(.databaseError(error: L10n.errorGettingDocuments))
                return
            }
            guard let documents = querySnapshot?.documents else {
                print("No projects found")
                completion([])
                return
            }
            let result: [Product] = documents.compactMap { (queryDocumentSnapshot: QueryDocumentSnapshot) -> Product? in
                return try? queryDocumentSnapshot.data(as: Product.self)
            }.sorted(by: { $0.category < $1.category })

            completion(result)
        }
    }

    static func addProducts() {
        print("Add Products")
        var products = [
            Product(name: "Hamburger menü", price: 15, image: "https://firebasestorage.googleapis.com/v0/b/papidu-5daca.appspot.com/o/products%2Fburger.png?alt=media&token=510aee68-f6ec-4df4-b455-279c1c06add1", category: L10n.menu),
            Product(name: "Miccs menü", price: 16, image: "https://firebasestorage.googleapis.com/v0/b/papidu-5daca.appspot.com/o/products%2Fmiccs_menu.png?alt=media&token=be7ce03f-f617-4a92-8b51-c5c4c4b5d716", category: L10n.menu),
            Product(name: "Kolbász menü", price: 16, image: "https://firebasestorage.googleapis.com/v0/b/papidu-5daca.appspot.com/o/products%2Fkolbasz_menu2.png?alt=media&token=6da30ee1-728c-44fc-b1f7-3804a77421e3", category: L10n.menu),
            Product(name: "Hamburger", price: 12, image: "https://firebasestorage.googleapis.com/v0/b/papidu-5daca.appspot.com/o/products%2Fburger.png?alt=media&token=510aee68-f6ec-4df4-b455-279c1c06add1", category: L10n.food),
            Product(name: "Hot Dog", price: 6, image: "https://firebasestorage.googleapis.com/v0/b/papidu-5daca.appspot.com/o/products%2Fhotdog2.png?alt=media&token=766b903c-99c0-4ee7-904d-c746e5a1640a", category: L10n.food),
            Product(name: "Miccs", price: 4, image: "https://firebasestorage.googleapis.com/v0/b/papidu-5daca.appspot.com/o/products%2Fmiccs.png?alt=media&token=38c86f7d-e316-40b0-8e86-f327eba722e3", category: L10n.food),
            Product(name: "Kolbász", price: 4, image: "https://firebasestorage.googleapis.com/v0/b/papidu-5daca.appspot.com/o/products%2Fkolbasz_menu2.png?alt=media&token=6da30ee1-728c-44fc-b1f7-3804a77421e3", category: L10n.food),
            Product(name: "Hagyma Karika", price: 5, image: "https://firebasestorage.googleapis.com/v0/b/papidu-5daca.appspot.com/o/products%2Fhagymakarikak.png?alt=media&token=f409e9f8-d961-4075-814d-5c0685e8fdd2", category: L10n.food),
            Product(name: "Szalmakrumpli", price: 5, image: "https://firebasestorage.googleapis.com/v0/b/papidu-5daca.appspot.com/o/products%2Fszalmakrumpli.png?alt=media&token=c1d458da-20c1-4c4b-b741-d70cb00ef1e6", category: L10n.food),
            Product(name: "Coca Cola 0.5L", price: 5, image: "https://firebasestorage.googleapis.com/v0/b/papidu-5daca.appspot.com/o/products%2Fcocacola.png?alt=media&token=469797a5-e173-4375-918d-22e7f2993f99", category: L10n.drink),
            Product(name: "Coca Cola 0.33L", price: 4, image: "https://firebasestorage.googleapis.com/v0/b/papidu-5daca.appspot.com/o/products%2Fcocacola.png?alt=media&token=469797a5-e173-4375-918d-22e7f2993f99", category: L10n.drink),
            Product(name: "Fanta 0.5L", price: 5, image: "https://firebasestorage.googleapis.com/v0/b/papidu-5daca.appspot.com/o/products%2Ffanta.png?alt=media&token=5aaa4653-7ac8-4cf6-b752-774dca0e45c4", category: L10n.drink),
            Product(name: "Fanta 0.33L", price: 4, image: "https://firebasestorage.googleapis.com/v0/b/papidu-5daca.appspot.com/o/products%2Ffanta.png?alt=media&token=5aaa4653-7ac8-4cf6-b752-774dca0e45c4", category: L10n.drink),
            Product(name: "Sprite 0.5L", price: 5, image: "https://firebasestorage.googleapis.com/v0/b/papidu-5daca.appspot.com/o/products%2Fsprite.png?alt=media&token=f597ef39-ad74-4c97-812a-03c068ae9503", category: L10n.drink),
            Product(name: "Prigat 0.5L", price: 5, image: "https://firebasestorage.googleapis.com/v0/b/papidu-5daca.appspot.com/o/products%2Fprigat.jpeg?alt=media&token=90063502-aced-42d4-8b59-1e7c56246a71", category: L10n.drink),
            Product(name: "Hell", price: 4, image: "https://firebasestorage.googleapis.com/v0/b/papidu-5daca.appspot.com/o/products%2Fhell.png?alt=media&token=fe7bc7bc-dc94-4753-9708-7f8bda2e6888", category: L10n.drink),
            Product(name: "Bucovina 0.5L", price: 5, image: "https://firebasestorage.googleapis.com/v0/b/papidu-5daca.appspot.com/o/products%2Fbucovina.png?alt=media&token=9c435f2a-2cd0-461c-a324-04ef752447a4", category: L10n.drink),
            Product(name: "Csapolt sör", price: 4, image: "https://firebasestorage.googleapis.com/v0/b/papidu-5daca.appspot.com/o/products%2Fcsapolt%20so%CC%88r.jpeg?alt=media&token=c6a09823-b049-4a0c-bf61-6fd37dcbb289", category: L10n.drink),
            Product(name: "Ursus", price: 5, image: "https://firebasestorage.googleapis.com/v0/b/papidu-5daca.appspot.com/o/products%2Fursus.png?alt=media&token=fd5b7708-78ad-4013-bee1-691a2b0c4aae", category: L10n.drink),
            Product(name: "Ursus 0%", price: 5, image: "https://firebasestorage.googleapis.com/v0/b/papidu-5daca.appspot.com/o/products%2Fursus.png?alt=media&token=fd5b7708-78ad-4013-bee1-691a2b0c4aae", category: L10n.drink),
            Product(name: "Ursus Cooler", price: 5, image: "https://firebasestorage.googleapis.com/v0/b/papidu-5daca.appspot.com/o/products%2FursusCooler.jpeg?alt=media&token=2d2144b2-6ab1-4ffa-95bc-a30fc30d16d6", category: L10n.drink),
            Product(name: "Heineken", price: 6, image: "https://firebasestorage.googleapis.com/v0/b/papidu-5daca.appspot.com/o/products%2Fheineken.png?alt=media&token=680cfd2f-3460-4d57-8164-0685db4f45b3", category: L10n.drink),
            Product(name: "Blue Kamikaze", price: 3, image: "https://firebasestorage.googleapis.com/v0/b/papidu-5daca.appspot.com/o/products%2Fblua%20kamikaze.jpeg?alt=media&token=f66ebdb6-15e0-4b8a-819c-77b43b77a363", category: L10n.shot),
            Product(name: "RaspBerry Kamikaze", price: 3, image: "https://firebasestorage.googleapis.com/v0/b/papidu-5daca.appspot.com/o/products%2Fraspberry%20kamikaze.jpeg?alt=media&token=e09b926e-4fdf-4f4e-9cbe-483b4313cbc2", category: L10n.shot),
            Product(name: "Tequila", price: 4, image: "https://firebasestorage.googleapis.com/v0/b/papidu-5daca.appspot.com/o/products%2Ftequila.jpeg?alt=media&token=e65d1858-5875-47a9-b93a-9a7e7a8df488", category: L10n.shot),
            Product(name: "Jagermaster", price: 4, image: "https://firebasestorage.googleapis.com/v0/b/papidu-5daca.appspot.com/o/products%2Fjagermaster.png?alt=media&token=164ee646-7e96-494c-be94-e704ef29ba74", category: L10n.shot),
            Product(name: "Unicum", price: 4, image: "https://firebasestorage.googleapis.com/v0/b/papidu-5daca.appspot.com/o/products%2Funicum.jpeg?alt=media&token=61d608db-ef88-42ed-af4a-ccc593ff83c4", category: L10n.shot),
            Product(name: "Summer Energy", price: 8, image: "https://firebasestorage.googleapis.com/v0/b/papidu-5daca.appspot.com/o/products%2Fsummerenergy.png?alt=media&token=1c804911-158f-4b3d-a9c4-c5812c424e0e", category: L10n.cocktail),
            Product(name: "Mojito", price: 8, image: "https://firebasestorage.googleapis.com/v0/b/papidu-5daca.appspot.com/o/products%2Fmohito.png?alt=media&token=57cb1c57-dec2-431e-b1d0-16ed657501f1", category: L10n.cocktail),
            Product(name: "Pina Colada", price: 8, image: "https://firebasestorage.googleapis.com/v0/b/papidu-5daca.appspot.com/o/products%2Fpinacolada.png?alt=media&token=42626510-84c5-4f7f-865e-08a1531c5f62", category: L10n.cocktail),
            Product(name: "Sex On The Beach", price: 8, image: "https://firebasestorage.googleapis.com/v0/b/papidu-5daca.appspot.com/o/products%2Fsexonthebeach.JPG?alt=media&token=a99e5bbf-c4e0-47a9-9069-8f0deb80ec70", category: L10n.cocktail),
            Product(name: "Gin Tonic", price: 8, image: "https://firebasestorage.googleapis.com/v0/b/papidu-5daca.appspot.com/o/products%2Fgintonic.png?alt=media&token=5af12907-fc1f-4ff6-b93f-38db76d6c0f1", category: L10n.cocktail),
            Product(name: "Cuba Libre", price: 8, image: "https://firebasestorage.googleapis.com/v0/b/papidu-5daca.appspot.com/o/products%2Fcubalibre.JPG?alt=media&token=8644ece8-d1cd-418f-b325-23e1c5e3a7f4", category: L10n.cocktail),
        ]

//        var product = Product(name: "Sprite", price: 5, image: "", category: L10n.drink)
//        let doc = productsCollectionRefference.document()
//        product.id = doc.documentID
//        doc.setData(product.dictionary)
        for product in products {
            var copyP = product
            let doc = productsCollectionRefference.document()
            copyP.id = doc.documentID
            doc.setData(copyP.dictionary)
        }

    }


    static func addToHistory(cartProducts: [ProductReport], failure: @escaping (IncrementError) -> Void) {

        var dictionary: [[String: Any]] = []
        cartProducts.forEach{ cartProduct in
            dictionary.append(cartProduct.dictionary)
        }

        let doc = salesHistory.document()
        let data: [String: Any] = [
            API.Param.id: doc.documentID,
            API.Param.productsReport: FieldValue.arrayUnion(dictionary),
            API.Param.dateTime: Date()
        ]

        doc.setData(data)
        { err in
            if err != nil {
                failure(.databaseError(error: "Couldn't add document"))
            }
        }
    }


    static func getProductReport(completion: @escaping ([ProductReport]) -> Void, failure: @escaping (IncrementError) -> Void) {

        getProducts(completion: { products in
            var productReport: [ProductReport] = []
            products.forEach{ product in
                productReport.append(ProductReport(product: product, quantity: 0))
            }
            salesHistory.getDocuments { querySnapshot, error in
                if let error = error {
                    print("Error getting documents: \(error)")
                    failure(.databaseError(error: L10n.errorGettingDocuments))
                    return
                }
                guard let documents = querySnapshot?.documents else {
                    print("No projects found")
                    completion([])
                    return
                }
                _ = documents.compactMap { (query) -> Void in
                    guard let history = try? query.data(as: SalesHistory.self) else {return}
                    print("sales h: \(history)")
                    for productRep in history.productsReport {
                        productReport.first(where: { report in report.product.id == productRep.product.id})?.quantity += productRep.quantity
                    }
                }

                completion(productReport)
            }
        }, failure: { _ in
            failure(.databaseError(error: L10n.errorGettingDocuments))
        })
    }

    static func getHistory(completion: @escaping ([SalesHistory]) -> Void,failure: @escaping (IncrementError) -> Void) {
        salesHistory.getDocuments { querySnapshot, error in
            if let error = error {
                print("Error getting documents: \(error)")
                failure(.databaseError(error: L10n.errorGettingDocuments))
                return
            }
            guard let documents = querySnapshot?.documents else {
                print("No projects found")
                completion([])
                return
            }
            let result: [SalesHistory] = documents.compactMap { (queryDocumentSnapshot: QueryDocumentSnapshot) -> SalesHistory? in
                return try? queryDocumentSnapshot.data(as: SalesHistory.self)
            }
            print(result)
            completion(result)
        }
    }

    static func deleteHistory(history: SalesHistory, completion: @escaping () -> Void, failure: @escaping (IncrementError) -> Void) {
        salesHistory.whereField(API.Param.id, isEqualTo: history.id).getDocuments() { (querySnapshot, err) in
            if let err = err {
              print("Error getting documents: \(err)")
                failure(.databaseError(error: L10n.errorGettingDocuments))
            } else {
                for document in querySnapshot!.documents {
                    document.reference.delete()
                }
                completion()
            }
        }
    }
}
