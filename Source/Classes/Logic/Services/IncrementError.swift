//
//  IncrementError.swift
//  Papidu
//
//  Created by Szekely Adrienn on 09.07.2021.
//

import Foundation

enum IncrementError: LocalizedError {
    case databaseError(error: String)
    case `default`(error: String)

    var errorDescription: String? {
        switch self {
        case let .databaseError(error):
            return error
        case let .default(error):
            return error
        }
    }
}
