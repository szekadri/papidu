//
//  WorkRepositoryProtocol.swift
//  Papidu
//
//  Created by Szekely Adrienn on 09.07.2021.
//

import Foundation

protocol WorkRepositoryProtocol {
    static func getProducts(completion: @escaping ([Product]) -> Void, failure: @escaping (IncrementError) -> Void)
    static func addProducts()
    static func addToHistory(cartProducts: [ProductReport], failure: @escaping (IncrementError) -> Void)
    static func deleteHistory(history: SalesHistory,completion: @escaping () -> Void,failure: @escaping (IncrementError) -> Void)
    static func getProductReport(completion: @escaping ([ProductReport]) -> Void,failure: @escaping (IncrementError) -> Void)
    static func getHistory(completion: @escaping ([SalesHistory]) -> Void,failure: @escaping (IncrementError) -> Void)
    static func getProducts(category: String, completion: @escaping ([Product]) -> Void, failure: @escaping (IncrementError) -> Void)
}
