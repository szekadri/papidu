//
//  AppDelegate.swift
//  Papidu
//
//  Created by Szekely Adrienn on 18.06.2021.
//

import UIKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?
    var flow: RootFlowController?

    var appEngine: AppEngine = AppEngine()

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]? = nil) -> Bool {
//        appEngine.prepareAppStart()
//        AppStyle.setupAppearance()

        window = UIWindow(frame: UIScreen.main.bounds)
        window?.makeKeyAndVisible()

        flow = RootFlowController(window: window)
        flow?.start()

        return true
    }
}

