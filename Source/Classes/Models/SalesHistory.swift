//
//  SalesHistory.swift
//  Papidu
//
//  Created by Szekely Adrienn on 10.07.2021.
//

import Foundation

struct SalesHistory: Codable {
    var id: String
    var dateTime: Date
    var productsReport: [ProductReport]

    enum CodingKeys: String, CodingKey {
        case dateTime
        case productsReport
        case id
    }
}
