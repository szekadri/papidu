//
//  Product.swift
//  Papidu
//
//  Created by Szekely Adrienn on 18.06.2021.
//

import Foundation

public struct Product: Codable, Equatable {
    var id: String = UUID.init().uuidString
    var name: String
    var price: Double
    var image: String
    var category: String

    public static func == (lhs: Product, rhs: Product) -> Bool {
        return (lhs.id == rhs.id)
    }

    var dictionary: [String: Any] {
        return [
            API.Param.id : id,
            API.Param.name : name,
            API.Param.category : category,
            API.Param.price : price,
            API.Param.image : image
        ]
    }
}
