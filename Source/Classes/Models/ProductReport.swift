//
//  ProductReport.swift
//  Papidu
//
//  Created by Szekely Adrienn on 10.07.2021.
//

import Foundation

class ProductReport: Codable, Equatable {

    var product: Product
    var quantity: Int

    init(product: Product, quantity: Int) {
        self.product = product
        self.quantity = quantity
    }

    enum CodingKeys: String, CodingKey {
        case product
        case quantity
    }

    var dictionary: [String: Any] {
        return [
            API.Param.product : product.dictionary,
            API.Param.quantity : quantity,
        ]
    }

    static func == (lhs: ProductReport, rhs: ProductReport) -> Bool {
        return lhs.product == rhs.product
    }
}
