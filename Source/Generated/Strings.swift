// swiftlint:disable all
// Generated using SwiftGen — https://github.com/SwiftGen/SwiftGen

import Foundation

// swiftlint:disable superfluous_disable_command file_length implicit_return

// MARK: - Strings

// swiftlint:disable function_parameter_count identifier_name line_length type_body_length
internal enum L10n {
  /// 
  internal static let blueKamikaze = L10n.tr("Localizable", "blue_kamikaze")
  /// cocktail
  internal static let cocktail = L10n.tr("Localizable", "cocktail")
  /// https://firebasestorage.googleapis.com/v0/b/papidu-5daca.appspot.com/o/products%2Fcsapolt%20so%CC%88r.jpeg?alt=media&token=c6a09823-b049-4a0c-bf61-6fd37dcbb289
  internal static func csapoltSör(_ p1: Float, _ p2: CChar) -> String {
    return L10n.tr("Localizable", "csapolt_sör", p1, p2)
  }
  /// 
  internal static let cubaLibre = L10n.tr("Localizable", "cuba_libre")
  /// yyyy.MM.dd
  internal static let dateFormat = L10n.tr("Localizable", "dateFormat")
  /// drink
  internal static let drink = L10n.tr("Localizable", "drink")
  /// Error getting documents: 
  internal static let errorGettingDocuments = L10n.tr("Localizable", "error_getting_documents")
  /// food
  internal static let food = L10n.tr("Localizable", "food")
  /// 
  internal static let ginTonic = L10n.tr("Localizable", "gin_tonic")
  /// https://firebasestorage.googleapis.com/v0/b/papidu-5daca.appspot.com/o/products%2Fhagymakarikak.png?alt=media&token=f409e9f8-d961-4075-814d-5c0685e8fdd2
  internal static func hagymaKarika(_ p1: Float) -> String {
    return L10n.tr("Localizable", "hagyma_karika", p1)
  }
  /// https://firebasestorage.googleapis.com/v0/b/papidu-5daca.appspot.com/o/products%2Fburger.png?alt=media&token=510aee68-f6ec-4df4-b455-279c1c06add1
  internal static func hamburger(_ p1: Float) -> String {
    return L10n.tr("Localizable", "hamburger", p1)
  }
  /// 
  internal static let hamburgerMenü = L10n.tr("Localizable", "hamburger_menü")
  /// https://firebasestorage.googleapis.com/v0/b/papidu-5daca.appspot.com/o/products%2Fheineken.png?alt=media&token=680cfd2f-3460-4d57-8164-0685db4f45b3
  internal static func heineken(_ p1: Float) -> String {
    return L10n.tr("Localizable", "heineken", p1)
  }
  /// https://firebasestorage.googleapis.com/v0/b/papidu-5daca.appspot.com/o/products%2Fhell.png?alt=media&token=fe7bc7bc-dc94-4753-9708-7f8bda2e6888
  internal static func hell(_ p1: Float) -> String {
    return L10n.tr("Localizable", "hell", p1)
  }
  /// https://firebasestorage.googleapis.com/v0/b/papidu-5daca.appspot.com/o/products%2Fhotdog2.png?alt=media&token=766b903c-99c0-4ee7-904d-c746e5a1640a
  internal static func hotDog(_ p1: Float) -> String {
    return L10n.tr("Localizable", "hot_dog", p1)
  }
  /// 
  internal static let jagermaster = L10n.tr("Localizable", "jagermaster")
  /// 
  internal static let kolbász = L10n.tr("Localizable", "kolbász")
  /// https://firebasestorage.googleapis.com/v0/b/papidu-5daca.appspot.com/o/products%2Fkolbasz_menu2.png?alt=media&token=6da30ee1-728c-44fc-b1f7-3804a77421e3
  internal static func kolbászMenü(_ p1: Float) -> String {
    return L10n.tr("Localizable", "kolbász_menü", p1)
  }
  /// menu
  internal static let menu = L10n.tr("Localizable", "menu")
  /// https://firebasestorage.googleapis.com/v0/b/papidu-5daca.appspot.com/o/products%2Fmiccs.png?alt=media&token=38c86f7d-e316-40b0-8e86-f327eba722e3
  internal static func miccs(_ p1: Float) -> String {
    return L10n.tr("Localizable", "miccs", p1)
  }
  /// https://firebasestorage.googleapis.com/v0/b/papidu-5daca.appspot.com/o/products%2Fmiccs_menu.png?alt=media&token=be7ce03f-f617-4a92-8b51-c5c4c4b5d716
  internal static func miccsMenü(_ p1: Float) -> String {
    return L10n.tr("Localizable", "miccs_menü", p1)
  }
  /// 
  internal static let mojito = L10n.tr("Localizable", "mojito")
  /// 
  internal static let pinaColada = L10n.tr("Localizable", "pina_colada")
  /// 
  internal static let raspberryKamikaze = L10n.tr("Localizable", "raspberry_kamikaze")
  /// 
  internal static let sexOnTheBeach = L10n.tr("Localizable", "sex_on_the_beach")
  /// shot
  internal static let shot = L10n.tr("Localizable", "shot")
  /// 
  internal static let summerEnergy = L10n.tr("Localizable", "summer_energy")
  /// https://firebasestorage.googleapis.com/v0/b/papidu-5daca.appspot.com/o/products%2Fszalmakrumpli.png?alt=media&token=c1d458da-20c1-4c4b-b741-d70cb00ef1e6
  internal static func szalmakrumpli(_ p1: Float) -> String {
    return L10n.tr("Localizable", "szalmakrumpli", p1)
  }
  /// 
  internal static let tequila = L10n.tr("Localizable", "tequila")
  /// 
  internal static let unicum = L10n.tr("Localizable", "unicum")
  /// https://firebasestorage.googleapis.com/v0/b/papidu-5daca.appspot.com/o/products%2Fursus.png?alt=media&token=fd5b7708-78ad-4013-bee1-691a2b0c4aae
  internal static func ursus(_ p1: Float) -> String {
    return L10n.tr("Localizable", "ursus", p1)
  }
  /// https://firebasestorage.googleapis.com/v0/b/papidu-5daca.appspot.com/o/products%2Fursus.png?alt=media&token=fd5b7708-78ad-4013-bee1-691a2b0c4aae
  internal static func ursus0(_ p1: Float) -> String {
    return L10n.tr("Localizable", "ursus_0%", p1)
  }
  /// https://firebasestorage.googleapis.com/v0/b/papidu-5daca.appspot.com/o/products%2FursusCooler.jpeg?alt=media&token=2d2144b2-6ab1-4ffa-95bc-a30fc30d16d6
  internal static func ursusCooler(_ p1: Float) -> String {
    return L10n.tr("Localizable", "ursus_cooler", p1)
  }
  /// https://firebasestorage.googleapis.com/v0/b/papidu-5daca.appspot.com/o/products%2Fbucovina.png?alt=media&token=9c435f2a-2cd0-461c-a324-04ef752447a4
  internal static func bucovina05L(_ p1: Float) -> String {
    return L10n.tr("Localizable", "bucovina_0.5L", p1)
  }
  /// https://firebasestorage.googleapis.com/v0/b/papidu-5daca.appspot.com/o/products%2Fcocacola.png?alt=media&token=469797a5-e173-4375-918d-22e7f2993f99
  internal static func cocaCola033L(_ p1: Float) -> String {
    return L10n.tr("Localizable", "coca_cola_0.33L", p1)
  }
  /// https://firebasestorage.googleapis.com/v0/b/papidu-5daca.appspot.com/o/products%2Fcocacola.png?alt=media&token=469797a5-e173-4375-918d-22e7f2993f99
  internal static func cocaCola05L(_ p1: Float) -> String {
    return L10n.tr("Localizable", "coca_cola_0.5L", p1)
  }
  /// https://firebasestorage.googleapis.com/v0/b/papidu-5daca.appspot.com/o/products%2Ffanta.png?alt=media&token=5aaa4653-7ac8-4cf6-b752-774dca0e45c4
  internal static func fanta033L(_ p1: Float) -> String {
    return L10n.tr("Localizable", "fanta_0.33L", p1)
  }
  /// https://firebasestorage.googleapis.com/v0/b/papidu-5daca.appspot.com/o/products%2Ffanta.png?alt=media&token=5aaa4653-7ac8-4cf6-b752-774dca0e45c4
  internal static func fanta05L(_ p1: Float) -> String {
    return L10n.tr("Localizable", "fanta_0.5L", p1)
  }
  /// https://firebasestorage.googleapis.com/v0/b/papidu-5daca.appspot.com/o/products%2Fprigat.jpeg?alt=media&token=90063502-aced-42d4-8b59-1e7c56246a71
  internal static func prigat05L(_ p1: Float) -> String {
    return L10n.tr("Localizable", "prigat_0.5L", p1)
  }
  /// https://firebasestorage.googleapis.com/v0/b/papidu-5daca.appspot.com/o/products%2Fsprite.png?alt=media&token=f597ef39-ad74-4c97-812a-03c068ae9503
  internal static func sprite05L(_ p1: Float) -> String {
    return L10n.tr("Localizable", "sprite_0.5L", p1)
  }
}
// swiftlint:enable function_parameter_count identifier_name line_length type_body_length

// MARK: - Implementation Details

extension L10n {
  private static func tr(_ table: String, _ key: String, _ args: CVarArg...) -> String {
    let format = BundleToken.bundle.localizedString(forKey: key, value: nil, table: table)
    return String(format: format, locale: Locale.current, arguments: args)
  }
}

// swiftlint:disable convenience_type
private final class BundleToken {
  static let bundle: Bundle = {
    #if SWIFT_PACKAGE
    return Bundle.module
    #else
    return Bundle(for: BundleToken.self)
    #endif
  }()
}
// swiftlint:enable convenience_type
